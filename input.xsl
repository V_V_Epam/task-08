<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.val.vol.98.gmail.com"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.val.col.98.gmail.com input.xsd">

<xsl:template match="/">
    <html>
        <body>
            <xsl:for-each select="flats/flat">
                <xsl:value-of select="address"/><br/>
                <xsl:value-of select="floor"/><br/>
                <xsl:value-of select="renovation/renovationType"/><br/>
                <xsl:value-of select="renovation/pipeType"/><br/>
                <xsl:value-of select="renovation/wiring/@measure"/><br/>
                <xsl:value-of select="renovation/wiring"/><br/>
                <br/>
            </xsl:for-each>
        </body>
    </html>

</xsl:template>
</xsl:stylesheet>