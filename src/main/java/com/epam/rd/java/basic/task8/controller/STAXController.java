package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import com.epam.rd.java.basic.task8.validation.Validation;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.Collections;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 * @author VVY
 * @version 1.0
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flats flats;
	private final String fileExtentionXSD = ".xsd";

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void readData() {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			Validation.validationXML(xmlFileName.substring(0, xmlFileName.indexOf(".")) + fileExtentionXSD, xmlFileName);

			// инициализируем reader и скармливаем ему xml файл
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			flats = new Flats();
			Flat flat = null;
			// проходим по всем элементам xml файла
			while (reader.hasNext()) {
				// получаем событие (элемент) и разбираем его по атрибутам
				String addressString = null;
				String floorString = null;
				int floorInt = 0;
				String renovationTypeString = null;
				String pipeTypeString = null;
				String wiringString = null;
				int wiringInt = 0;
				String measureString = null;
				XMLEvent xmlEvent = reader.nextEvent();
				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					if (startElement.getName().getLocalPart().equals("flat")) {
						flat = new Flat();
					}
					if (startElement.getName().getLocalPart().equals("address")) {
						xmlEvent = reader.nextEvent();
						addressString = xmlEvent.asCharacters().getData();
						flat.setAddress(addressString);
						//System.out.println(addressString);
					}
					if (startElement.getName().getLocalPart().equals("floor")) {
						xmlEvent = reader.nextEvent();
						floorString = xmlEvent.asCharacters().getData();
						floorInt = Integer.parseInt(floorString);
						flat.setFloor(floorInt);
						//System.out.println(floorString);
					}
					if (startElement.getName().getLocalPart().equals("renovationType")) {
						xmlEvent = reader.nextEvent();
						renovationTypeString = xmlEvent.asCharacters().getData();
						//System.out.println(renovationTypeString);
						RenovationTypeEnum renovationType = RenovationTypeEnum.fromValue(renovationTypeString);
						flat.setRenovationType(renovationType);
					}
					if (startElement.getName().getLocalPart().equals("pipeType")) {
						xmlEvent = reader.nextEvent();
						pipeTypeString = xmlEvent.asCharacters().getData();
						PipeTypeEnum pipeType = PipeTypeEnum.fromValue(pipeTypeString);
						flat.setPipeType(pipeType);
						//System.out.println(pipeTypeString);
						//System.out.println(flat);
					}
					if (startElement.getName().getLocalPart().equals("wiring")) {
						xmlEvent = reader.nextEvent();
						wiringString = xmlEvent.asCharacters().getData();
						wiringInt = Integer.parseInt(wiringString);
						measureString = startElement.getAttributeByName(new QName("measure")).getValue();
						Wiring wiring = new Wiring(wiringInt, measureString);
						flat.setWiring(wiring);
						//System.out.println(wiringInt);
						//System.out.println(measureString);
					}
				}

				// если цикл дошел до закрывающего элемента flat,
				// то добавляем считанную из файла квартиру в список
				if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flat")) {
						flats.getFlats().add(flat);
					}
				}
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		//System.out.println(flats);
	}

	public void sortData() {
		Collections.sort(flats.getFlats(), new Comparator<Flat>() {
			@Override
			public int compare(Flat o1, Flat o2) {
				return o1.getRenovation().getRenovationType().getType().
						compareTo(o2.getRenovation().getRenovationType().getType());
			}
		});
		//System.out.println(flats);
	}

	public void saveData(String outputXmlFile) {
		XMLOutputFactory output = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = null;
		try {
			writer = output.createXMLStreamWriter(new FileOutputStream(outputXmlFile));

			writer.writeStartDocument("UTF-8", "1.0");

			//flats
			writer.writeStartElement("flats");
			writer.writeAttribute("xmlns:xsi",
						"http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xmlns",
						"http://www.val.vol.98.gmail.com");
			writer.writeAttribute("xsi:schemaLocation",
						"http://www.val.col.98.gmail.com input.xsd");

			//flat
			for (Flat f : flats.getFlats()) {
				writer.writeStartElement("flat");

				writer.writeStartElement("address");
				writer.writeCharacters(f.getAddress());
				writer.writeEndElement();

				writer.writeStartElement("floor");
				writer.writeCharacters(f.getFloor() + "");
				writer.writeEndElement();

				writer.writeStartElement("renovation");
				writer.writeStartElement("renovationType");
				writer.writeCharacters(f.getRenovation().getRenovationType().getType());
				writer.writeEndElement();

				writer.writeStartElement("pipeType");
				writer.writeCharacters(f.getRenovation().getPipeType().getType());
				writer.writeEndElement();

				writer.writeStartElement("wiring");
				writer.writeAttribute("measure", Wiring.getMeasure());
				writer.writeCharacters(f.getRenovation().getWiring().getValue() + "");
				writer.writeEndElement();

				//renovation
				writer.writeEndElement();

				//flat
				writer.writeEndElement();
			}
			//flats
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.flush();
					writer.close();
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			}
		}
	}
}