package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import com.epam.rd.java.basic.task8.validation.Validation;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.Collections;
import java.util.Comparator;

/**
 * Controller for SAX parser.
 * @author VVY
 * @version 1.0
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private Flats flats;
	private final String fileExtentionXSD = ".xsd";

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void readData() {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new SaxHandler();

			Validation.validationXML(xmlFileName.substring(0, xmlFileName.indexOf(".")) + fileExtentionXSD, xmlFileName);

			File file = new File(xmlFileName);
			flats = new Flats();
			saxParser.parse(file, handler);
			//System.out.println(flats);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sortData() {
		Collections.sort(flats.getFlats(), new Comparator<Flat>() {
			@Override
			public int compare(Flat o1, Flat o2) {
				return o2.getAddress().compareTo(o1.getAddress());
			}
		});
		//System.out.println(flats);
	}

	public void saveData(String outputXmlFile) {
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
		XMLStreamWriter xmlStreamWriter = null;
		try {
			xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(new FileOutputStream(outputXmlFile), "UTF-8");
			xmlStreamWriter.writeStartDocument("UTF-8", "1.0");

			xmlStreamWriter.writeStartElement("flats");
			xmlStreamWriter.writeAttribute("xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance");
			xmlStreamWriter.writeAttribute("xmlns",
					"http://www.val.vol.98.gmail.com");
			xmlStreamWriter.writeAttribute("xsi:schemaLocation",
					"http://www.val.col.98.gmail.com input.xsd");

			for (Flat f : flats.getFlats()) {
				xmlStreamWriter.writeStartElement("flat");
				xmlStreamWriter.writeStartElement("address");
				xmlStreamWriter.writeCharacters(f.getAddress());
				xmlStreamWriter.writeEndElement();//address

				xmlStreamWriter.writeStartElement("floor");
				xmlStreamWriter.writeCharacters(f.getFloor()+"");
				xmlStreamWriter.writeEndElement();//floor

				xmlStreamWriter.writeStartElement("renovation");
				xmlStreamWriter.writeStartElement("renovationType");
				xmlStreamWriter.writeCharacters(f.getRenovation().getRenovationType().getType());
				xmlStreamWriter.writeEndElement();//renovationType

				xmlStreamWriter.writeStartElement("pipeType");
				xmlStreamWriter.writeCharacters(f.getRenovation().getPipeType().getType());
				xmlStreamWriter.writeEndElement();//pipeType

				xmlStreamWriter.writeStartElement("wiring");
				xmlStreamWriter.writeAttribute("measure", Wiring.getMeasure());
				xmlStreamWriter.writeCharacters(f.getRenovation().getWiring().getValue() + "");
				xmlStreamWriter.writeEndElement();//wiring

				xmlStreamWriter.writeEndElement();//renovation

				xmlStreamWriter.writeEndElement();//flat
			}
			xmlStreamWriter.writeEndDocument();//flats

			xmlStreamWriter.writeEndDocument();

		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (xmlStreamWriter != null) {
				try {
					xmlStreamWriter.flush();
					xmlStreamWriter.close();
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			}
		}

	}

	class SaxHandler extends DefaultHandler {
		boolean foundAddress = false;
		boolean foundFloor = false;
		boolean foundRenovationType = false;
		boolean foundPipeType = false;
		String attributeWiringMeasure;
		boolean foundWiring = false;

		String addressString;
		String floorString;
		String renovationTypeString;
		String pipeTypeString;
		String wiringString;
		String wiringMeasureString;

		@Override
		public void startElement(String uri, String localName, String qName,
								 Attributes attributes) throws SAXException {

			//System.out.println("---------------------------");
			///System.out.println("Start Element :" + qName);

			if (qName.equals("address")) {
				foundAddress = true;
			}

			if (qName.equals("floor")) {
				foundFloor = true;
			}

			if (qName.equals("renovationType")) {
				foundRenovationType = true;
			}

			if (qName.equals("pipeType")) {
				foundPipeType = true;
			}

			if (qName.equals("wiring")) {
				attributeWiringMeasure = attributes.getValue("measure");
				foundWiring = true;
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			if (qName.equals("flat")) {
				RenovationTypeEnum renovationType = RenovationTypeEnum.fromValue(renovationTypeString);
				PipeTypeEnum pipeType = PipeTypeEnum.fromValue(pipeTypeString);
				Wiring wiring = new Wiring(Integer.parseInt(wiringString), wiringMeasureString);

				Renovation renovation = new Renovation(renovationType, pipeType, wiring);
				Flat flat = new Flat(addressString, Integer.parseInt(floorString), renovation);
				flats.getFlats().add(flat);
			}
			//System.out.println("End Element :" + qName);
		}

		@Override
		public void characters(char ch[], int start, int length)
				throws SAXException {

			//System.out.println("characters :");
			if (foundAddress) {
				addressString = new String(ch, start, length);
				//System.out.println("Address : " + new String(ch, start, length));
				foundAddress = false;
			}

			if (foundFloor) {
				floorString = new String(ch, start, length);
				//System.out.println("Floor : " + new String(ch, start, length));
				foundFloor = false;
			}

			if (foundRenovationType) {
				renovationTypeString = new String(ch, start, length);
				//System.out.println("RenovationType : " + new String(ch, start, length));
				foundRenovationType = false;
			}

			if (foundPipeType) {
				pipeTypeString = new String(ch, start, length);
				//System.out.println("pipeType : " + new String(ch, start, length));
				foundPipeType = false;
			}

			if (foundWiring) {
				wiringString = new String(ch, start, length);
				wiringMeasureString = attributeWiringMeasure;
				//System.out.println("wiring : " + attributeWiringMeasure + " " + new String(ch, start, length));
				foundWiring = false;
			}
		}
	}
}

