package com.epam.rd.java.basic.task8.validation;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Validator to chack XML file according to XSD file.
 * @author VVY
 * @version 1.0
 */
public class Validation {

    /**
     * function to do validation XML file according to XSD file
     * @param schemaXSDFileName - file name of the *.xsd file
     * @param xmlFileName - file name of the *.xml file
     * @throws SAXException - exception of load schema and procedure of validate *xml file
     * @throws IOException - exception procedure of validate *xml file
     */
    public static void validationXML(String schemaXSDFileName, String xmlFileName) throws SAXException, IOException {
        Schema schema = null;
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        schema = factory.newSchema(new File(schemaXSDFileName));
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(new File(xmlFileName)));
    }}
