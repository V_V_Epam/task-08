package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.validation.Validation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.*;
import com.epam.rd.java.basic.task8.entity.*;

/**
 * Controller for DOM parser.
 * @author VVY
 * @version 1.0
 */
public class DOMController {

	private String xmlFileName;
	private final String fileExtentionXSD = ".xsd";
	private Flats flats;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void readData() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();

			Validation.validationXML(xmlFileName.substring(0, xmlFileName.indexOf(".")) + fileExtentionXSD, xmlFileName);

			Document document = builder.parse(xmlFileName);
			Element rootFlats = document.getDocumentElement();

			NodeList flatNodes =
					rootFlats.getElementsByTagName("flat");
			flats = new Flats();
			for (int j = 0; j < flatNodes.getLength(); j++) {
				Node flatNode = flatNodes.item(j);
				Element elementFlat = (Element) flatNode;
				String address = elementFlat.getElementsByTagName("address").item(0).getTextContent();
				int floor = Integer.parseInt(elementFlat.getElementsByTagName("floor").item(0).getTextContent());

				Node renovationNode = elementFlat.getElementsByTagName("renovation").item(0);
				Element elementRenovation = (Element) renovationNode;

				String renovationTypeString = elementRenovation.getElementsByTagName("renovationType").item(0).getTextContent();
				RenovationTypeEnum renovationType = RenovationTypeEnum.fromValue(renovationTypeString);

				String pipeTypeString = elementRenovation.getElementsByTagName("pipeType").item(0).getTextContent();
				PipeTypeEnum pipeType = PipeTypeEnum.fromValue(pipeTypeString);
				int valueWiring = Integer.parseInt(elementRenovation.getElementsByTagName("wiring").item(0).getTextContent());

				Node wiringNode = elementRenovation.getElementsByTagName("wiring").item(0);
				Element elementWiring = (Element) wiringNode;
				String measureWiring = elementWiring.getAttribute("measure");
				Wiring wiring = new Wiring(valueWiring, measureWiring);

				Renovation renovation = new Renovation(renovationType, pipeType, wiring);

				Flat flat = new Flat(address, floor, renovation);

				/*Node node = flatNodes.item(j);
				Element element = (Element) node;
				System.out.print(element.getElementsByTagName("address").item(0).getTextContent() + " - ");
				System.out.print(element.getElementsByTagName("floor").item(0).getTextContent() + " - ");

				Node node1 = element.getElementsByTagName("renovation").item(0);
				Element el = (Element) node1;
				System.out.print(el.getElementsByTagName("renovationType").item(0).getTextContent() + " - ");
				System.out.print(el.getElementsByTagName("pipeType").item(0).getTextContent() + " - ");
				System.out.print(el.getElementsByTagName("wiring").item(0).getTextContent() + " - ");

				Node node2 = el.getElementsByTagName("wiring").item(0);
				Element elem = (Element) node2;
				System.out.println(elem.getAttribute("measure"));*/
				flats.getFlats().add(flat);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		//System.out.println(flats);
	}

	public void sortData() {
		Collections.sort(flats.getFlats(), new Comparator<Flat>() {
			@Override
			public int compare(Flat o1, Flat o2) {
				return o1.getFloor() - o2.getFloor();
			}
		});
		//System.out.println(flats);
	}

	public void saveData(String outputXmlFile) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();

			// создаем пустой объект Document, в котором будем
			// создавать наш xml-файл
			Document doc = builder.newDocument();

			// создаем корневой элемент
			Element rootElement = doc.createElement("flats");
			rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootElement.setAttribute("xmlns", "http://www.val.vol.98.gmail.com");
			rootElement.setAttribute("xsi:schemaLocation", "http://www.val.col.98.gmail.com input.xsd");

			doc.appendChild(rootElement);
			for (Flat f : flats.getFlats()) {
				Element flat = doc.createElement("flat");
				Element address = doc.createElement("address");
				address.appendChild(doc.createTextNode(f.getAddress()));
				Element floor = doc.createElement("floor");
				floor.appendChild(doc.createTextNode(f.getFloor() + ""));
				Element renovation = doc.createElement("renovation");
				Element renovationType = doc.createElement("renovationType");
				renovationType.appendChild(doc.createTextNode(f.getRenovation().getRenovationType().getType()));
				Element pipeType = doc.createElement("pipeType");
				pipeType.appendChild(doc.createTextNode(f.getRenovation().getPipeType().getType()));
				Element wiring = doc.createElement("wiring");
				wiring.setAttribute("measure", Wiring.getMeasure());
				wiring.appendChild(doc.createTextNode(f.getRenovation().getWiring().getValue() + ""));

				rootElement.appendChild(flat);
				flat.appendChild(address);
				flat.appendChild(floor);
				flat.appendChild(renovation);
				renovation.appendChild(renovationType);
				renovation.appendChild(pipeType);
				renovation.appendChild(wiring);
			}

			//создаем объект TransformerFactory для печати в файл
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMSource source = new DOMSource(doc);
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			//печатаем в файл
			StreamResult file = new StreamResult(new File(outputXmlFile));

			//записываем данные
			transformer.transform(source, file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}