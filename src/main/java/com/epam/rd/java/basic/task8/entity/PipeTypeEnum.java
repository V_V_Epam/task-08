package com.epam.rd.java.basic.task8.entity;

/**
 * Entity to save object of the pipeType.
 * @author VVY
 * @version 1.0
 */
public enum PipeTypeEnum {
    PLASTIC ("plastic"),
    METAL ("metal"),
    CAST_IRON ("cast iron");
    private String type;

    PipeTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    /**
     * function to get enum of the PipeTypeEnum
     * @param givenName - string value
     * @return - value of PipeTypeEnum
     */
    public static PipeTypeEnum fromValue(String givenName) {
        for (PipeTypeEnum pte : PipeTypeEnum.values()) {
            if (pte.type.equals(givenName)) {
                return pte;
            }
        }
        return null;
    }
}

