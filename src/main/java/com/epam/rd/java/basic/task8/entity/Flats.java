package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity to save object of the flats.
 * @author VVY
 * @version 1.0
 */
public class Flats {
    private List<Flat> flats;

    public Flats() {
        this.flats = new ArrayList<>();
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public void setFlats(List<Flat> flats) {
        this.flats = flats;
    }

    @Override
    public String toString() {
        StringBuilder information = new StringBuilder();
        information.append("Information about all the flats \n");
        for (Flat flat : flats) {
            information.append(flat.toString());
        }
        return new String(information);
    }
}
