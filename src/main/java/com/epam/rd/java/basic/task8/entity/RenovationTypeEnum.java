package com.epam.rd.java.basic.task8.entity;

/**
 * Entity to save object of the renovationType.
 * @author VVY
 * @version 1.0
 */
public enum RenovationTypeEnum {
    EURO ("euro"),
    COSMETIC ("cosmetic"),
    ELITE ("elite");
    private String type;

    RenovationTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    /**
     * function to get enum of the RenovationTypeEnum
     * @param givenName - string value
     * @return - value of RenovationTypeEnum
     */
    public static RenovationTypeEnum fromValue(String givenName) {
        for (RenovationTypeEnum rte : RenovationTypeEnum.values()) {
            if (rte.type.equals(givenName)) {
                return rte;
            }
        }
        return null;
    }
}
