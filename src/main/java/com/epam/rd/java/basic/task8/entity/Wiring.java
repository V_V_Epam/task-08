package com.epam.rd.java.basic.task8.entity;

/**
 * Entity to save object of the wiring.
 * @author VVY
 * @version 1.0
 */
 public class Wiring {
    private static String measure = "mmxmm";
    private int value;

    public Wiring(int value, String measureLocal) {
        measure = measureLocal;
        this.value = value;
    }

    public Wiring() {

    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static String getMeasure() {
        return measure;
    }

    public static void setMeasure(String measureLocal) {
        measure = measureLocal;
    }

    @Override
    public String toString() {
        return "     [wiring: value = " + value + "; measure = " + measure + "]";
    }
}