package com.epam.rd.java.basic.task8.entity;

/**
 * Entity to save object of the renovation.
 * @author VVY
 * @version 1.0
 */
public class Renovation {
    private RenovationTypeEnum renovationType;
    private PipeTypeEnum pipeType;
    private Wiring wiring;

    public Renovation(RenovationTypeEnum renovationType, PipeTypeEnum pipeType, Wiring wiring) {
        this.renovationType = renovationType;
        this.pipeType = pipeType;
        this.wiring = wiring;
    }

    public Renovation() {
        wiring = new Wiring();
    }

    public RenovationTypeEnum getRenovationType() {
        return renovationType;
    }

    public void setRenovationType(RenovationTypeEnum renovationType) {
        this.renovationType = renovationType;
    }

    public PipeTypeEnum getPipeType() {
        return pipeType;
    }

    public void setPipeType(PipeTypeEnum pipeType) {
        this.pipeType = pipeType;
    }

    public Wiring getWiring() {
        return wiring;
    }

    public void setWiring(Wiring wiring) {
        this.wiring = wiring;
    }

    @Override
    public String toString() {
        return "     [renovation: renovationType = " + renovationType.getType() +
                "; pipeType = " + pipeType.getType() +
                "]; \n" + wiring.toString();
    }
}
