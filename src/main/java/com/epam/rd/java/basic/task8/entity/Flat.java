package com.epam.rd.java.basic.task8.entity;

/**
 * Entity to save object of the flat.
 * @author VVY
 * @version 1.0
 */
public class Flat {
    private String address;
    private int floor;
    private Renovation renovation;

    public Flat() {
        renovation = new Renovation();
    }

    public Flat(String address, int floor, Renovation renovation) {
        this.address = address;
        this.floor = floor;
        this.renovation = renovation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public Renovation getRenovation() {
        return renovation;
    }

    public void setRenovation(Renovation renovation) {
        this.renovation = renovation;
    }

    public void setRenovationType(RenovationTypeEnum renovationType) {
        this.renovation.setRenovationType(renovationType);
    }

    public void setPipeType(PipeTypeEnum pipeType) {
        this.renovation.setPipeType(pipeType);
    }

    public void setWiring(Wiring wiring) {
        this.renovation.setWiring(wiring);
    }

    @Override
    public String toString() {
        return "[flat: address = " + address +
                "; floor = " + floor +
                "]; \n" + renovation.toString() + "\n";
    }
}
